# major version of Implant
VERSION=1

GOPATH = $(shell echo $$GOPATH)
CWD = $(shell pwd)

BUILD_DATE := `date +%Y%m%d%-H%M`
LDFLAGS=-ldflags "-X gitlab.com/oumy.com/implant/services/config.BUILD_DATE='$(BUILD_DATE)'"
VERSIONFILE := pkg/config/build.go

# Generate the protocol buffer classes
proto-gen:
#	protoc --proto_path=. --go_out=plugins=grpc:. services/sim/simprotocol/simprotocol.proto
#	protoc --proto_path=. --go_out=plugins=grpc:. services/recommender/defs.proto

gen-build-file:
	touch $(VERSIONFILE)

# Build the code for local use. -v lists which packages are recompiled
build: proto-gen gen-build-file
	go install $(LDFLAGS) -v ./...

# Cross compile to linux binaries, and copy executables to bin folder
# To install the cross compilation for golang: brew install go --with-cc-common 
# http://blog.dimroc.com/2015/08/20/cross-compiled-go-with-docker/
build-linux: proto-gen gen-build-file
	rm -f bin/*
	env GOOS=linux GOARCH=amd64 go install $(LDFLAGS) -v ./...
	mkdir -p bin
	cp $(GOPATH)/bin/linux_amd64/* bin

# Install all dependent packages
dependencies:
	go get ./...

run: build
