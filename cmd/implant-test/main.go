package main

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/oumy.com/implant/pkg/flag"
	"gitlab.com/oumy.com/implant/pkg/logger"
)

var log = logger.New()

var limit = flag.Int("limit", 1000*1000*1000, "Limit the number of records to analyze")

func main() {
	log.Printf("This is a test, %d", limit)

	log.WithFields(logrus.Fields{
		"animal": "walrus",
	}).Info("A walrus appears")

	log.Warn("This is a warning")
}
