package main

import (
	_ "fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/oumy.com/implant/pkg/flag"
	"gitlab.com/oumy.com/implant/pkg/logger"
	"gitlab.com/oumy.com/implant/pkg/scraper"
	"net/url"
	"strings"
)

var log = logger.New()

// var siteRoot = flag.String("siteRoot", "https://www.irs.gov/newsroom/irs-withholding-tables-frequently-asked-questions", "Root URL of the site to scrape")

// var siteRoot = flag.String("siteRoot", "https://www.irs.gov/newsroom/tax-relief-in-disaster-situations", "Root URL of the site to scrape")

var siteRoot = flag.String("siteRoot", "https://www.irs.gov/help/telephone-assistance", "Root URL of the site to scrape")

type Tool struct {
	crawler *scraper.Crawler
}

func NewTool() *Tool {
	t := &Tool{}
	t.crawler = scraper.NewCrawler()
	t.crawler.Sites["www.irs.gov"] = t
	return t
}

func (t *Tool) Run() {
	t.crawler.Push(siteRoot)
	t.crawler.Crawl(1, 1)
}

func (t *Tool) ShouldCrawlUrl(u *url.URL) bool {
	if strings.HasPrefix(u.Path, "/zh-hant/") {
		return false
	} else if strings.HasPrefix(u.Path, "/ko/") {
		return false
	} else if strings.HasPrefix(u.Path, "/es/") {
		return false
	} else if strings.HasPrefix(u.Path, "/ru/") {
		return false
	} else if strings.HasPrefix(u.Path, "/vi/") {
		return false
	}

	if strings.HasSuffix(u.Path, ".pdf") {
		return false
	}

	return true
}

func (t *Tool) ProcessPage(u *url.URL, node *scraper.Node) error {

	pageTitle := node.FindTag("h1").GetText()

	log.WithFields(logrus.Fields{
		"url":         u.String(),
		"title":       node.FindTag("title").GetText(),
		"h1":          node.FindTag("h1").GetText(),
		"h2":          node.FindTag("h2").GetText(),
		"has-article": node.FindTag("article").IsTag(),
	}).Info("page-processed")

	article := node.FindTag("article")
	article.Remove()

	if article != nil {
		// make a new root
		na := scraper.NewNode("div", nil)

		h := scraper.NewNode("h1", nil)
		h.Text = pageTitle

		na.AddChild(h)
		na.AddChild(article)
		article = na

		tr := scraper.NewTransformer()
		article = tr.Transform(article)

		tagger := scraper.NewTagger()
		tagger.Tag(article)

		pr := &scraper.NodePrinter{
			FullPath:             true,
			IndentLevels:         true,
			IncludeIDInPath:      true,
			IncludeClassesInPath: false,
		}
		pr.Print(article)
	}

	/*
		tagger := scraper.NewTagger()
		tagger.Tag(node)

		pr := &scraper.NodePrinter{}
		pr.Print(node)
	*/

	return nil
}

func main() {
	log.Printf("Implant Web Site Scraper V0.1")

	t := NewTool()
	t.Run()
}
