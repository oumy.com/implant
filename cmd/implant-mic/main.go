package main

import (
	"gitlab.com/oumy.com/implant/pkg/audio/mic"
	"gitlab.com/oumy.com/implant/pkg/audio/rev"
	"gitlab.com/oumy.com/implant/pkg/logger"
	"os"
	"os/signal"
	"syscall"
)

var log = logger.New()

type Tool struct {
	sttEngine    *rev.Engine
	mic          *mic.Mic
	gracefulStop chan os.Signal
	Done         chan bool
}

func NewTool() (*Tool, error) {
	var err error

	t := &Tool{
		gracefulStop: make(chan os.Signal),
		Done:         make(chan bool),
	}

	var frameLength int32 = 512
	var sampleRate int32 = 16000

	signal.Notify(t.gracefulStop, syscall.SIGTERM)
	signal.Notify(t.gracefulStop, syscall.SIGINT)

	t.sttEngine, err = rev.New("test", sampleRate)
	if err != nil {
		return nil, err
	}

	t.mic, err = mic.New(frameLength, sampleRate)

	return t, nil
}

func (t *Tool) RunLoop() {
	for {
		select {
		case <-t.Done:
			break
		case <-t.gracefulStop:
			t.Shutdown()
			break
		case buf := <-t.mic.AudioStream:
			t.sttEngine.ProcessAudio(buf)
		}
	}
}

func (t *Tool) Shutdown() {
	log.Printf("Shutting down ...")

	t.sttEngine.Shutdown()
	t.mic.Shutdown()

	log.Printf("Shut down")

	t.Done <- true
}

func (t *Tool) WaitForShutdown() {
	<-t.Done
}

func main() {
	log.Printf("Implant - Microphone booting up ...")

	t, err := NewTool()
	if err != nil {
		log.Fatalf("Unable to start %s", err)
	}

	go t.RunLoop()
	t.WaitForShutdown()

	log.Printf("Implant Exiting.")
}
