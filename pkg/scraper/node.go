package scraper

// Represent a node in an HTML document

import (
	"strings"
)

type Node struct {
	Parent *Node
	Next   *Node // sibling link
	Child  *Node // to list of children

	Tag     string // empty if a text node
	Id      string
	Attr    map[string]string
	Classes []string
	Href    string
	Text    string

	// Fields used by the tagger
	Hash  string
	Depth int // in tree
}

func NewNode(tag string, parent *Node) *Node {
	return &Node{
		Parent: parent,
		Tag:    tag,
		Attr:   make(map[string]string),
	}
}

func NewTextNode(text string, parent *Node) *Node {
	return &Node{
		Parent: parent,
		Text:   text,
		Attr:   make(map[string]string),
	}
}

func (n *Node) AddAttr(name string, value string) {
	if name == "id" {
		n.Id = value
	} else if name == "class" {
		for _, class := range strings.Split(value, " ") {
			n.Classes = appendStrs(n.Classes, class)
		}
	} else if name == "href" || name == "src" {
		n.Href = value
	} else {
		n.Attr[name] = value
	}
}
