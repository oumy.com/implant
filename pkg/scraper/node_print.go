package scraper

import (
	"bytes"
	"fmt"
	"github.com/fatih/color"
)

var (
	txtcol   = color.New(color.FgYellow)
	aliascol = color.New(color.FgRed)
	tagcol   = color.New(color.FgGreen)
	hrefcol  = color.New(color.FgCyan)
	pathcol  = color.New(color.FgWhite)
)

type NodePrinter struct {
	FullPath             bool
	IncludeClassesInPath bool
	IncludeIDInPath      bool
	IndentLevels         bool
	root                 *Node
}

func (printer *NodePrinter) Print(n *Node) {
	printer.root = n

	printer.print(n)
}

func (printer *NodePrinter) print(n *Node) {
	if n == nil {
		return
	}

	var printNode bool

	/*
		if n.Tag == "" && n.Text != "" {
			printNode = true
		}
		if n.Tag == "title" {
			printNode = true
		}
	*/

	if n.Text != "" {
		printNode = true
	}

	if printNode {
		if n.Hash != "" {
			aliascol.Printf("%s", n.Hash)
		}

		txtcol.Printf(" %d", n.Depth)
		if printer.IndentLevels {
			txtcol.Printf(" ")

			lvl := n.Depth
			for lvl > 0 {
				txtcol.Printf("---")
				lvl--
			}
		}

		txtcol.Printf(" %s", n.GetText())

		if n.Href != "" {
			hrefcol.Printf(" %s", n.Href)
		}

		if printer.FullPath {
			pathcol.Printf(" %s", printer.PathString(n))
		} else {
			pathcol.Printf(" %s", n.GetTag())
		}

		pathcol.Printf("\n")
	}

	printChildren := true
	if n.Tag == "script" || n.Tag == "noscript" || n.Tag == "style" {
		printChildren = false
	}

	if printChildren {
		p := n.Child
		for p != nil {
			printer.print(p)
			p = p.Next
		}
	}
}

func (printer *NodePrinter) PathString(n *Node) string {
	var buf bytes.Buffer

	e := n
	for e != nil {
		if buf.Len() > 0 {
			buf.WriteString(".")
		}

		s := printer.Stringify(e)
		if s != "" {
			buf.WriteString(s)
		}

		if e == printer.root {
			break
		}

		e = e.Parent
	}

	return buf.String()
}

func (printer *NodePrinter) Stringify(n *Node) string {
	var buf bytes.Buffer

	if n.Tag != "" {
		buf.WriteString(fmt.Sprintf("%s", n.Tag))
	} else {
		buf.WriteString("@")
	}

	if printer.IncludeIDInPath && n.Id != "" {
		buf.WriteString(fmt.Sprintf("[%s]", n.Id))
	}

	if printer.IncludeClassesInPath {
		buf.WriteString(printer.StringifyClasses(n))
	}

	return buf.String()
}

func (printer *NodePrinter) StringifyClasses(n *Node) string {
	var buf bytes.Buffer

	for _, class := range n.Classes {

		if true {
			buf.WriteString(fmt.Sprintf("(%s)", class))
		}
	}

	return buf.String()
}
