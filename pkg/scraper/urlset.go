package scraper

// Implements a simple set of Urls that is ordered

type UrlSet struct {
	member   map[string]*UrlNode
	sentinel *UrlNode
	size     int
}

type UrlNode struct {
	url        string
	prev, next *UrlNode
}

func NewUrlSet() *UrlSet {
	sentinel := &UrlNode{
		url: "",
	}
	sentinel.next = sentinel
	sentinel.prev = sentinel

	return &UrlSet{
		member:   make(map[string]*UrlNode),
		sentinel: sentinel,
	}
}

func (s *UrlSet) Size() int {
	return s.size
}

func (s *UrlSet) Contains(url string) bool {
	_, ok := s.member[url]
	return ok
}

func (s *UrlSet) Insert(url string) {
	if s.Contains(url) {
		return // already a member of the set
	}

	n := &UrlNode{
		url: url,
	}

	s.member[url] = n
	s.sentinel.insertBefore(n) // insert at end of linked list
	s.size++
}

func (s *UrlSet) Remove(url string) {
	n, ok := s.member[url]
	if !ok {
		return // not a member
	}

	n.unlink()
	delete(s.member, url)
	s.size--
}

func (s *UrlSet) Pop() (string, bool) {
	if s.size == 0 {
		return "", false
	}

	n := s.sentinel.next // pick first in the list

	n.unlink()
	delete(s.member, n.url)
	s.size--

	return n.url, true
}

// Insert n after p
func (p *UrlNode) insertAfter(n *UrlNode) {
	nxt := p.next

	n.prev = p
	n.next = nxt
	p.next = n
	nxt.prev = n
}

func (p *UrlNode) insertBefore(n *UrlNode) {
	p.prev.insertAfter(n)
}

func (p *UrlNode) unlink() {
	prev := p.prev
	nxt := p.next

	prev.next = nxt
	nxt.prev = prev

	p.next = nil
	p.prev = nil
}
