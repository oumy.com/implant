package scraper

// sets up logging for the web scraper

import (
	"gitlab.com/oumy.com/implant/pkg/logger"
)

var log = logger.New()
