package scraper

import (
	"github.com/sirupsen/logrus"
	"net/url"
	"runtime"
	"sync"
)

type Crawler struct {
	Sites        map[string]Site
	FrontierUrls *UrlSet
	PendingUrls  *UrlSet
	VisitedUrls  *UrlSet
	lock         sync.Mutex
	workerQueue  chan *url.URL
}

type Site interface {
	ShouldCrawlUrl(url *url.URL) bool
	ProcessPage(url *url.URL, node *Node) error
}

func NewCrawler() *Crawler {
	return &Crawler{
		Sites:        make(map[string]Site),
		FrontierUrls: NewUrlSet(),
		PendingUrls:  NewUrlSet(),
		VisitedUrls:  NewUrlSet(),
		workerQueue:  make(chan *url.URL),
	}
}

func (c *Crawler) Push(href string) {
	c.lock.Lock()
	defer c.lock.Unlock()

	if href == "" {
		return
	}

	pageUrl, err := url.Parse(href)
	if err != nil {
		log.WithFields(logrus.Fields{
			"url": href,
		}).Error("crawler-ignore-invalid-url")

		return
	}

	site, ok := c.Sites[pageUrl.Host]
	if !ok {
		return // not a site to crawl
	}

	if !site.ShouldCrawlUrl(pageUrl) {
		return
	}

	if c.VisitedUrls.Contains(href) {
		return // dont revisit web pages
	}

	c.FrontierUrls.Insert(href)
}

// gets the next URL and also marks it visited
func (c *Crawler) NextUrl() string {
	c.lock.Lock()
	defer c.lock.Unlock()

	url, ok := c.FrontierUrls.Pop()

	if ok {
		c.PendingUrls.Insert(url)
		return url
	} else {
		return ""
	}
}

func (c *Crawler) MarkVisited(url string) {
	c.lock.Lock()
	defer c.lock.Unlock()

	c.PendingUrls.Remove(url)
	c.VisitedUrls.Insert(url)
}

func (c *Crawler) Crawl(noOfWorkers int, maxNoPages int) error {
	log.WithFields(logrus.Fields{
		"no-of-workers": noOfWorkers,
		"max-pages":     maxNoPages,
	}).Info("crawler-start")

	// start workers
	var wg sync.WaitGroup
	wg.Add(noOfWorkers)

	for i := 0; i < noOfWorkers; i++ {
		go c.workerLoop(&wg)
	}

	c.mainLoop(maxNoPages)

	wg.Wait()

	return nil
}

func (c *Crawler) mainLoop(maxNoPages int) {
	noPages := 0
	for {
		if noPages >= maxNoPages {
			break
		}

		runtime.Gosched()

		pageUrl := c.NextUrl()
		if pageUrl == "" {
			if c.PendingUrls.Size() == 0 {
				break // no more page to process
			} else {
				continue
			}
		}

		u, _ := url.Parse(pageUrl)
		c.workerQueue <- u

		if noPages%100 == 0 {
			log.WithFields(logrus.Fields{
				"visited-pages":  c.VisitedUrls.Size(),
				"frontier-pages": c.FrontierUrls.Size(),
			}).Info("crawler-status")
		}

		noPages++
	}

	close(c.workerQueue)

	log.WithFields(logrus.Fields{
		"no-pages":      noPages,
		"frontier-size": c.FrontierUrls.Size(),
	}).Info("crawler-mainloop-stop")
}

func (c *Crawler) workerLoop(wg *sync.WaitGroup) {
	defer wg.Done()

	log.Info("crawl-worker-start")

	fetcher := NewFetcher()

	for u := range c.workerQueue {
		// Check if this is a site we support
		site, ok := c.Sites[u.Host]
		if !ok {
			log.WithFields(logrus.Fields{
				"url":  u.String(),
				"host": u.Host,
			}).Error("crawler-site-configuration-not-found")
			continue
		}

		// Fetch the page
		node, err := fetcher.Fetch(u)
		if err != nil {
			log.WithFields(logrus.Fields{
				"url":   u.String(),
				"error": err,
			}).Error("crawler-worker-page-fetch-error")

			continue
		}

		// Perform the page processing
		err = site.ProcessPage(u, node)
		if err != nil {
			log.WithFields(logrus.Fields{
				"url":   u.String(),
				"error": err,
			}).Error("Page processing error")
		}

		// Extract all the hyperlinks <a> to follow to next pages
		node.WalkTag("a", func(n *Node) {
			href := n.Href
			if href == "" {
				return
			}

			// resolve the URL in the context of the page
			refUrl, err := u.Parse(href)
			if err != nil {
				log.WithFields(logrus.Fields{
					"page": u.String(),
					"ref":  href,
				}).Error("Referenced link is not a valid URL")

				return
			}

			refUrl.Fragment = "" // kill off any fragment

			c.Push(refUrl.String())
		})

		c.MarkVisited(u.String())
	}

	log.Info("crawl-worker-stop")
}
