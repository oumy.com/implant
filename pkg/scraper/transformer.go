package scraper

type Transformer struct {
}

func NewTransformer() *Transformer {
	t := &Transformer{}

	return t
}

func (t *Transformer) Transform(n *Node) *Node {
	return t.copyTransform(n)
}

func (t *Transformer) copyTransform(n *Node) *Node {
	if n == nil {
		return nil
	}

	c := n.ShallowCopy()

	// copy and transform all children
	p := n.Child
	for p != nil {
		cp := t.copyTransform(p)
		c.AddChild(cp)
		p = p.Next
	}

	return t.transform(c)
}

func (t *Transformer) transform(n *Node) *Node {
	if n.Tag == "script" {
		return nil // remove
	}

	{ // remove empty nodes
		if n.Text == "" && n.Tag != "" && n.NoChildren() == 0 && n.Tag != "br" {
			return nil
		}
	}

	{ // role renaming
		role := n.GetAttr("role")
		if role != "" {
			n.Tag = role
		}
	}

	{ // text lifting into parent
		tc := n.OnlyChild("")
		if tc != nil && n.Tag != "div" {
			n.Text = tc.GetText()
			n.RemoveChildren()
		}
	}

	{ // <a> lifting into parent
		a := n.OnlyChild("a")
		if a != nil && a.Text != "" {
			n.Text = a.Text
			n.Href = a.Href
			n.RemoveChildren()
		}
	}

	if false { // remove simple <div> <div> nestings
		d := n.OnlyChild("div")
		if n.Tag == "div" && d != nil {
			n = d
		}
	}

	{ // translate <p><strong></p> into <h>
		s := n.OnlyChild("strong")
		if n.Tag == "p" && s != nil {
			n.Tag = "h"
			n.Text = s.GetText()
			n.RemoveChildren()
		}
	}

	{ // Collapse spans of many children of <p>, <li> maintaining <a> children
		if (n.Tag == "p" || n.Tag == "li") && n.NoChildren() > 1 && n.Href == "" {
			nn := NewNode(n.Tag, nil)
			nn.Text = n.GetFlattenedText()

			nl := NewNodeList()
			n.Walk(func(p *Node) {
				if p.Href != "" {
					nl.Append(p)
				}
			})

			p := nl.PopFirst()
			for p != nil {
				nn.AddChild(p)
				p = nl.PopFirst()
			}

			n = nn
		}
	}

	{ // <ul> with all links gets converted to <sitenav>
		if n.Tag == "ul" {
			noChildren := 0
			noLinks := 0

			p := n.Child
			for p != nil {
				noChildren++
				if p.Href != "" {
					noLinks++
				}
				p = p.Next
			}

			if noChildren == noLinks {
				n.Tag = "sitenav"
			}
		}
	}

	{
		// <a> with single child, we return the child and copy the href. handles <a><h></a> situations
		if n.Tag == "a" && n.NoChildren() == 1 && n.Href != "" {
			nn := n.Child
			nn.Href = n.Href
			n = nn
		}

		// collapse content of <a>
		if n.Tag == "a" && n.NoChildren() > 0 && n.Href != "" {
			n.Text = n.GetFlattenedText()
			n.RemoveChildren()
		}
	}

	if false {
		_, h := n.IsHeader()
		if h {
			n.Text = n.GetFlattenedText()
			n.RemoveChildren()
		}
	}

	{ // Header nesting
		if n.NoChildren() > 1 {
			hs := NewHStack(n)
			n = hs.Reorg()
		}
	}

	return n
}
