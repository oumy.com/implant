package scraper

import (
	"fmt"
)

func containsStrs(strs []string, value string) bool {
	for _, s := range strs {
		if s == value {
			return true
		}
	}
	return false
}

func appendStrs(strs []string, value string) []string {
	for _, s := range strs {
		if s == value {
			return strs
		}
	}
	return append(strs, value)
}

func removeStrs(strs []string, value string) []string {
	if containsStrs(strs, value) {
		var r []string

		for _, s := range strs {
			if s != value {
				r = append(r, s)
			}
		}

		return r
	} else {
		return strs
	}

}

func appendStr(x string, y string) string {
	if x == "" {
		return y
	} else if y == "" {
		return x
	} else {
		return fmt.Sprintf("%s, %s", x, y)
	}
}
