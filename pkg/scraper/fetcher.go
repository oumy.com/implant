package scraper

import (
	"net/http"
	"net/url"
)

const (
	// Mobile Phone User agent returns simpler web pages
	USER_AGENT_MOBILE  = "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1"
	USER_AGENT_DESKTOP = "Oumy Crawler/1.0"
)

type Fetcher struct {
	client *http.Client
}

func NewFetcher() *Fetcher {
	client := &http.Client{}

	return &Fetcher{
		client: client,
	}
}

func (f *Fetcher) Fetch(u *url.URL) (*Node, error) {
	req, err := f.buildGet(u)
	if err != nil {
		return nil, err
	}

	resp, err := f.client.Do(req)
	if err != nil {
		return nil, err
	}

	body := resp.Body
	defer body.Close()

	parser := NewHtmlParser()
	node, err := parser.Parse(u, body)
	if err != nil {
		return nil, err
	}

	return node, nil
}

func (f *Fetcher) buildGet(u *url.URL) (*http.Request, error) {
	req, err := http.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("User-Agent", USER_AGENT_DESKTOP)
	req.Header.Add("Accept", "text/html; charset=UTF-8")
	req.Header.Add("Cache-Control", "max-age=0")
	req.Header.Add("Pragma", "no-cache")
	req.Header.Add("Host", req.Host)
	req.Header.Add("Accept-Language", "en,en-US;q=0.9")
	// req.Header.Add("Referer", url)

	return req, nil
}
