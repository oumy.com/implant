package scraper

// Manages H1-5 tags

type HStack struct {
	top     *HNode
	oldroot *Node
	newroot *Node
}

type HNode struct {
	n     *Node
	level int
	next  *HNode
}

func NewHStack(root *Node) *HStack {
	newroot := root.ShallowCopy()

	p := &HNode{
		n:     newroot,
		level: 0,
	}
	return &HStack{
		top:     p,
		oldroot: root,
		newroot: newroot,
	}
}

func (hs *HStack) Reorg() *Node {
	p := hs.oldroot.Child
	for p != nil {
		nxt := p.Next
		// fmt.Printf("reorg %s\n", p.Tag)
		hs.Push(p)
		p = nxt
	}

	return hs.newroot
}

func (hs *HStack) Top() *Node {
	return hs.top.n
}

func (hs *HStack) Level() int {
	return hs.top.level
}

func (hs *HStack) Push(p *Node) {
	_, isHeader := p.IsHeader()
	if isHeader {
		hs.PushHeader(p)
	} else {
		// simply push onto top
		// fmt.Printf("simple push %s onto %s\n", p.Tag, hs.Top().Tag)
		hs.Top().AddChild(p)
	}
}

func (hs *HStack) PushHeader(p *Node) {
	level, isHeader := p.IsHeader()
	if !isHeader {
		return
	}

	if level == 0 { // generic "h" tag so uplevel
		if hs.Top().GetTag() == "h" {
			level = hs.Level()
		} else {
			level = hs.Level() + 1
		}
	}

	if level == hs.Level() {
		hs.pop()
		hs.Top().AddChild(p)
		hs.push(p, level)
	} else if level > hs.Level() {
		hs.Top().AddChild(p)
		hs.push(p, level)
	} else if level < hs.Level() {
		for level <= hs.Level() {
			hs.pop()
		}
		hs.Top().AddChild(p)
		hs.push(p, level)
	}
}

func (hs *HStack) push(n *Node, level int) {
	hn := &HNode{
		n:     n,
		level: level,
		next:  hs.top,
	}

	hs.top = hn
}

func (hs *HStack) pop() {
	if hs.top != nil && hs.top.next != nil {
		hs.top = hs.top.next
	}
}
