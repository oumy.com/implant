package scraper

// Basic list of nodes

type NodeList struct {
	n []*Node
}

func NewNodeList() *NodeList {
	return &NodeList{
		n: make([]*Node, 0),
	}
}

func (l *NodeList) Len() int {
	return len(l.n)
}

func (l *NodeList) ElemAt(i int) *Node {
	return l.n[i]
}

func (l *NodeList) Append(n *Node) {
	l.n = append(l.n, n)
}

func (l *NodeList) PopFirst() *Node {
	if len(l.n) == 0 {
		return nil
	}

	r := l.n[0]
	l.n = l.n[1:]

	return r
}

func (l *NodeList) PopLast() *Node {
	if len(l.n) == 0 {
		return nil
	}

	r := l.n[len(l.n)-1]
	l.n = l.n[:len(l.n)-1]

	return r
}
