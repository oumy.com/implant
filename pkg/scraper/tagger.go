package scraper

import (
	"crypto/md5"
	"fmt"
)

type Tagger struct {
	node    *Node
	printer *NodePrinter
}

func NewTagger() *Tagger {
	return &Tagger{
		printer: &NodePrinter{},
	}
}

func (t *Tagger) Tag(n *Node) {
	t.node = n

	t.tag(n)
}

func (t *Tagger) tag(n *Node) {
	if n == nil {
		return
	}

	// calc hash for the node
	path := t.printer.PathString(n)
	n.Hash = t.hashPath(path)[0:10]

	n.Depth = t.depth(n)

	t.tagNode(n)
	t.tagTextNode(n)

	p := n.Child
	for p != nil {
		t.tag(p)
		p = p.Next
	}
}

func (t *Tagger) tagNode(n *Node) {
}

func (t *Tagger) tagTextNode(n *Node) {
	if n.Tag != "" || n.Text == "" {
		return // not a text node
	}
}

func (t *Tagger) hashPath(pathString string) string {
	buf := []byte(pathString)
	return fmt.Sprintf("%x", md5.Sum(buf))
}

func (t *Tagger) depth(n *Node) int {
	d := 0

	p := n
	for p != nil && p != t.node {
		if p.Text != "" {
			d++
		}
		p = p.Parent
	}

	return d - 1
}
