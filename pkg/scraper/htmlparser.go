package scraper

import (
	"bytes"
	"fmt"
	"golang.org/x/net/html"
	"io"
	"net/url"
	"strings"
	"unicode"
)

type HtmlParser struct {
	tokenizer *html.Tokenizer
	tokenType html.TokenType
	token     html.Token
}

func NewHtmlParser() *HtmlParser {
	return &HtmlParser{}
}

func (s *HtmlParser) Parse(u *url.URL, reader io.Reader) (*Node, error) {
	s.tokenizer = html.NewTokenizer(reader)
	s.tokenizer.AllowCDATA(false)

	s.advance()

	n := s.parseElement(nil)

	var err error
	if s.isEOF() {
	} else if s.isError() {
		err = s.tokenizer.Err()
	} else if n == nil {
		err = fmt.Errorf("Empty HTML document")
	}

	return n, err
}

func (s *HtmlParser) parseElement(parent *Node) *Node {
	if s.isError() {
		return nil
	}

	// log.Println("parse", s.tokenType, s.token.Data)

	if s.isText() {
		var buf bytes.Buffer

		for s.isText() {
			buf.WriteString(s.getText())
			s.advance()
		}

		n := NewTextNode(buf.String(), parent)
		return n
	} else if s.isBeginEnd() {
		n := NewNode(s.getTag(), parent)

		s.updateAttrs(n)

		s.advance()
		return n
	} else if s.isStart() {
		tag := s.getTag()
		n := NewNode(tag, parent)

		s.updateAttrs(n)

		s.advance()

		if s.allowsChildren(tag) {
			var sibling *Node
			for {
				// log.Println("parse2", s.tokenType, s.token.Data)

				if s.isEOF() || s.isError() {
					break
				} else if s.isEndTag(tag) {
					s.advance()
					break
				} else if s.isEnd() {
					// log.Printf("-------- Skipping expected end tag: %s", s.getTag())
					s.advance()
					break
				}

				child := s.parseElement(n)
				if child == nil {
					continue
				}

				// update the list of children
				if sibling == nil { // first child
					n.Child = child
				} else { // have a sibling
					sibling.Next = child
				}

				sibling = child
			}
		}

		return n
	} else {
		log.Printf("----------Skipping unknown tag: %s", s.tokenType)
		s.advance()
	}

	return nil
}

func (s *HtmlParser) allowsChildren(tag string) bool {
	if tag == "link" || tag == "meta" || tag == "input" {
		return false
	}

	return true
}

func (s *HtmlParser) advance() {
	for {
		s.tokenType = s.tokenizer.Next()
		s.token = s.tokenizer.Token()

		if s.isError() || s.isEOF() {
			return
		} else if s.tokenType == html.CommentToken {
		} else if s.tokenType == html.DoctypeToken {
		} else if s.tokenType == html.TextToken {
			// filter out empty text tokens
			t := strings.TrimFunc(s.token.Data, trimFunc)
			if t != "" {
				return
			}
		} else {
			return
		}
	}
}

func (s *HtmlParser) updateAttrs(n *Node) {
	for _, a := range s.token.Attr {
		n.AddAttr(a.Key, a.Val)
	}
}

func (s *HtmlParser) isError() bool {
	return s.tokenType == html.ErrorToken
}

func (s *HtmlParser) isEOF() bool {
	return s.tokenType == html.ErrorToken && s.tokenizer.Err() == io.EOF
}

func (s *HtmlParser) isStart() bool {
	return s.tokenType == html.StartTagToken
}

func (s *HtmlParser) isEnd() bool {
	return s.tokenType == html.EndTagToken
}

func (s *HtmlParser) isBeginEnd() bool {
	return s.tokenType == html.SelfClosingTagToken
}

func (s *HtmlParser) isStartTag(tag string) bool {
	return s.tokenType == html.StartTagToken && s.token.Data == tag
}

func (s *HtmlParser) isEndTag(tag string) bool {
	return s.tokenType == html.EndTagToken && s.token.Data == tag
}

func (s *HtmlParser) isBeginEndTag(tag string) bool {
	return s.tokenType == html.SelfClosingTagToken && s.token.Data == tag
}

func (s *HtmlParser) isText() bool {
	return s.tokenType == html.TextToken
}

func (s *HtmlParser) getTag() string {
	if s.isStart() || s.isEnd() || s.isBeginEnd() {
		return s.token.Data
	} else {
		return ""
	}
}

func (s *HtmlParser) getText() string {
	if s.isText() {
		return strings.TrimFunc(s.token.Data, noTrimFunc)
	} else {
		return ""
	}
}

/*
func (s *HtmlParser) getAttr(attr string) string {
	for _, a := range s.token.Attr {
		if a.Key == attr {
			return a.Val
		}
	}

	return ""
}
*/

func trimFunc(r rune) bool {
	if r <= ' ' {
		return true
	} else if unicode.IsSpace(r) {
		return true
	} else if unicode.IsControl(r) {
		return true
	} else if r == 0x0085 || r == 0x2028 || r == 0x2029 || r == 0x2085 { // newlines
		return true
	} else if r == 0x2008 || r == 0x2009 || r == 0x200A || r == 0x200B || r == 0xfeff { // spaces
		return true
	} else if r == '-' || r == '|' || r == ',' {
		return true
	}

	return false
}

func spaceTrimFunc(r rune) bool {
	return r <= ' '
}

func noTrimFunc(r rune) bool {
	if unicode.IsControl(r) {
		return true
	}

	return false
}
