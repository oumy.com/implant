package scraper

import (
	"bytes"
	"strings"
)

func (n *Node) NoChildren() int {
	count := 0

	p := n.Child
	for p != nil {
		count++
		p = p.Next
	}
	return count
}

func (n *Node) IsTag() bool {
	if n == nil {
		return false
	}
	return n.Tag != ""
}

func (n *Node) IsText() bool {
	if n == nil {
		return false
	}
	return n.Text != ""
}

func (n *Node) GetTag() string {
	if n == nil {
		return ""
	}

	e := n
	for e != nil {
		if e.Tag != "" {
			return e.Tag
		}

		e = e.Parent
	}

	return ""
}

func (n *Node) GetText() string {
	if n == nil {
		return ""
	}

	return strings.TrimFunc(n.Text, spaceTrimFunc)
}

func (n *Node) GetFlattenedText() string {
	if n == nil {
		return ""
	}

	var buffer bytes.Buffer

	n.Walk(func(p *Node) {
		if p.Tag == "br" {
			buffer.WriteString(" ")
		} else if p.Href != "" && p.Text != "" {
			buffer.WriteString(p.Text)
		} else if p.Text != "" {
			buffer.WriteString(p.Text)
		}
	})

	return strings.TrimFunc(buffer.String(), spaceTrimFunc)
}

/*
func (n *Node) GetMarkedText() string {
	if n == nil {
		return ""
	}

	var buffer bytes.Buffer

	buffer.WriteString("{")

	n.Walk(func(p *Node) {
		if p.Tag == "br" {
			buffer.WriteString(" ")
		} else if p.Href != "" && p.Text != "" {
			buffer.WriteString("[")
			buffer.WriteString(p.Text)
			buffer.WriteString("|")
			buffer.WriteString(p.Href)
			buffer.WriteString("]")
		} else if p.Text != "" {
			buffer.WriteString(p.Text)
		}
	})

	buffer.WriteString("}")

	return buffer.String()
}
*/

func (n *Node) GetAttr(name string) string {
	if n == nil {
		return ""
	}

	if n.Attr == nil {
		return ""
	}

	return n.Attr[name]
}

func (n *Node) GetHref() string {
	if n == nil {
		return ""
	}

	e := n
	for e != nil {
		if e.Href != "" {
			return e.Href
		}

		e = e.Parent
	}

	return ""
}

// Find all instance of a tag
func (n *Node) WalkTag(tag string, callback func(p *Node)) {
	n.Walk(func(p *Node) {
		if p.Tag == tag {
			callback(p)
		}
	})
}

// Find all instance of a class
func (n *Node) WalkClass(class string, callback func(p *Node)) {
	n.Walk(func(p *Node) {
		if containsStrs(p.Classes, class) {
			callback(p)
		}
	})
}

// pre-order traversal starting at root
func (n *Node) Walk(callback func(p *Node)) {
	if n == nil {
		return
	}

	callback(n)

	// visit children
	n = n.Child
	for n != nil {
		n.Walk(callback)
		n = n.Next
	}
}

// find all tags
func (n *Node) FindTags(tag string) *NodeList {
	list := NewNodeList()
	n.WalkTag(tag, func(p *Node) {
		list.Append(p)
	})
	return list
}

// find all nodes with class
func (n *Node) FindClasses(class string) *NodeList {
	list := NewNodeList()
	n.WalkClass(class, func(p *Node) {
		list.Append(p)
	})
	return list
}

// find first tag
func (n *Node) FindTag(tag string) *Node {
	var result *Node

	n.WalkTag(tag, func(p *Node) {
		if result == nil {
			result = p
		}
	})

	return result
}

// find first node with class
func (n *Node) FindClass(class string) *Node {
	var result *Node

	n.WalkClass(class, func(p *Node) {
		if result == nil {
			result = p
		}
	})

	return result
}

// if n has one child of tag then return it
func (n *Node) OnlyChild(tag string) *Node {
	ch := n.Child

	if ch != nil && ch.Next == nil && ch.Tag == tag {
		return ch
	} else {
		return nil
	}
}

func (n *Node) ShallowCopy() *Node {
	c := &Node{
		Id:      n.Id,
		Tag:     n.Tag,
		Text:    n.Text,
		Href:    n.Href,
		Classes: n.Classes,
		Attr:    n.Attr,
		Parent:  nil,
		Child:   nil,
		Next:    nil,
	}

	return c
}

func (n *Node) RemoveChildren() {
	n.Child = nil
}

func (n *Node) AddChild(c *Node) {
	if c == nil {
		return
	}

	c.Next = nil
	c.Parent = n

	if n.Child == nil {
		n.Child = c
	} else {
		p := n.Child
		for p.Next != nil {
			p = p.Next
		}
		p.Next = c
	}
}

func (n *Node) Remove() {
	if n.Parent == nil {
		return
	}

	parent := n.Parent
	p := parent.Child

	var prev *Node = nil
	for p != nil {
		nxt := p.Next

		if p == n {
			if prev == nil {
				parent.Child = nxt
			} else {
				prev.Next = nxt
			}
			n.Next = nil
			n.Parent = nil
			return
		}

		prev = p
		p = p.Next
	}

}

func (n *Node) IsHeader() (level int, ok bool) {
	switch n.Tag {
	case "h":
		return 0, true
	case "h1":
		return 1, true
	case "h2":
		return 2, true
	case "h3":
		return 3, true
	case "h4":
		return 4, true
	case "h5":
		return 5, true
	default:
		return 0, false
	}
}
