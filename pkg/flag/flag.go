package flag

// Replacement for standard golang flag package, which has major issues with flags
// distributed over multiple modules/initialization order.

// Flags are of the format:
// --flag=value
// --flag

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path"
	"regexp"
)

var (
	regexprFlagVal = regexp.MustCompile("^--([A-Za-z0-9-_]*)=(.*)$")
	regexprFlag    = regexp.MustCompile("^--([A-Za-z0-9-_]*)$")
)

var flags FlagMap

func init() {
	args := parseCmdLineArguments()

	env := args.String("env", "", "")
	configFile := args.String("config", "", "")

	// A shortcut to use the executable name to locate the config file
	if env != "" && configFile == "" {
		executable := path.Base(os.Args[0])
		configFile = fmt.Sprintf("%s-config.json", executable)
	}

	cfg := make(FlagMap) // empty
	if configFile != "" {
		cfg = loadConfig(configFile, env) // overwrite
	}

	flags = make(FlagMap)
	flags.Merge(cfg)
	flags.Merge(args)

	// flags.Log()
}

func loadConfig(filename, envname string) FlagMap {
	fm := make(FlagMap)

	file, err := os.Open(filename)
	if err != nil {
		log.Fatalf("unable to open config file %s: %v", filename, err)
	}

	decoder := json.NewDecoder(file)

	var model map[string]interface{}
	err = decoder.Decode(&model)
	if err != nil {
		log.Fatalf("unable to parse config file %s: %v", filename, err)
	}

	envs, ok := model["env"].(map[string]interface{})
	if !ok {
		log.Fatalf("no 'env' section found in config %s: %v", filename, err)
	}

	env, ok := envs[envname].(map[string]interface{})
	if !ok {
		log.Fatalf("no '%s' section found in config %s: %v", envname, filename, err)
	}

	for flag, value := range env {
		s := fmt.Sprintf("%v", value)
		fm.SetFlag(flag, s)
	}

	return fm
}

func parseCmdLineArguments() FlagMap {
	fm := make(FlagMap)

	args := os.Args

	i := 1
	for {
		if i >= len(args) {
			break
		}

		flag := args[i]

		// Style --Flag=Value
		match := regexprFlagVal.FindStringSubmatch(flag)
		if match != nil {
			fm.SetFlag(match[1], match[2])

			i++
			continue
		}

		// Style --Flag
		match = regexprFlag.FindStringSubmatch(flag)
		if match != nil {
			fm.SetFlag(match[1], "true")

			i++
			continue
		}

		log.Fatalf("Invalid commandline flag %s (only --flag=x or --flag is allowed)", flag)
	}

	return fm
}

func RequireString(flag string, description string) string {
	return flags.RequireString(flag, description)
}

func String(flag string, defaultValue string, description string) string {
	return flags.String(flag, defaultValue, description)
}

func RequireBool(flag string, description string) bool {
	return flags.RequireBool(flag, description)
}

func Bool(flag string, defaultValue bool, description string) bool {
	return flags.Bool(flag, defaultValue, description)
}

func RequireInt(flag string, description string) int {
	return flags.RequireInt(flag, description)
}

func Int(flag string, defaultValue int, description string) int {
	return flags.Int(flag, defaultValue, description)
}
