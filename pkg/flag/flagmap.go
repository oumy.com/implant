package flag

import (
	"log"
	"os/user"
	"path/filepath"
	"strconv"
)

type FlagMap map[string]*FlagVal

type FlagVal struct {
	Flag  string
	Value string
}

func (fm FlagMap) SetFlag(flag, value string) {
	fm[flag] = &FlagVal{
		Flag:  flag,
		Value: expand(value),
	}
}

func expand(value string) string {
	// Handle home directories
	if len(value) > 2 && value[:2] == "~/" {
		usr, _ := user.Current()
		dir := usr.HomeDir
		value = filepath.Join(dir, value[2:])
	}

	return value
}

func (fm FlagMap) Merge(f FlagMap) {
	for _, fv := range f {
		fm.SetFlag(fv.Flag, fv.Value)
	}
}

func (fm FlagMap) Log() {
	log.Printf("Flags:\n")

	for _, fv := range fm {
		log.Printf("Flag %s = %s\n", fv.Flag, fv.Value)
	}
}

func (fm FlagMap) String(flag string, defaultValue string, description string) string {
	fv, ok := fm[flag]
	if ok {
		return fv.Value
	} else {
		return expand(defaultValue)
	}
}

func (fm FlagMap) RequireString(flag string, description string) string {
	fv, ok := fm[flag]
	if ok {
		return fv.Value
	} else {
		log.Fatalf("This application requires a string flag %s (%s)", flag, description)
	}

	return ""
}

func (fm FlagMap) Bool(flag string, defaultValue bool, description string) bool {
	fv, ok := fm[flag]
	if ok {
		b, err := strconv.ParseBool(fv.Value)
		if err != nil {
			return false
		}
		return b
	} else {
		return defaultValue
	}
}

func (fm FlagMap) RequireBool(flag string, description string) bool {
	fv, ok := fm[flag]
	if ok {
		b, err := strconv.ParseBool(fv.Value)
		if err != nil {
			log.Fatalf("This application requires a bool flag %s (%s)", flag, description)
		}
		return b
	} else {
		log.Fatalf("This application requires a bool flag %s (%s)", flag, description)
	}

	return false
}

func (fm FlagMap) Int(flag string, defaultValue int, description string) int {
	fv, ok := fm[flag]
	if ok {
		x, err := strconv.Atoi(fv.Value)
		if err != nil {
			return defaultValue
		} else {
			return x
		}
	} else {
		return defaultValue
	}
}

func (fm FlagMap) RequireInt(flag string, description string) int {
	fv, ok := fm[flag]
	if ok {
		x, err := strconv.Atoi(fv.Value)
		if err != nil {
			log.Fatalf("This application requires an int flag %s (%s)", flag, description)
		} else {
			return x
		}
	}

	log.Fatalf("This application requires an int flag %s (%s)", flag, description)

	return 0
}
