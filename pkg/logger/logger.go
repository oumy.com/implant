package logger

import (
	"fmt"
	"github.com/lestrrat/go-file-rotatelogs"
	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
	"github.com/ventu-io/go-shortid"
	"gitlab.com/oumy.com/implant/pkg/flag"
	"os"
	"path"
	"runtime"
	"time"
)

var (
	thelog    *logrus.Logger
	processId string
)

var (
	env       = flag.String("env", "local", "the environment to run in, e.g. dev, prod")
	logToFile = flag.Bool("log-to-file", true, "log to the file system?")
	logdir    = flag.String("log-dir", "/tmp", "the directory where to write log files")
	loglevel  = flag.String("log-level", "info", "the level to log at e.g. trace, debug, info, warn, error")
)

func init() {
	processId, _ = shortid.Generate()

	thelog = logrus.New()

	// https://github.com/sirupsen/logrus/issues/63
	if false {
		thelog.SetReportCaller(true)
	}

	switch loglevel {
	case "debug":
		thelog.Level = logrus.DebugLevel
	case "info":
		thelog.Level = logrus.InfoLevel
	case "warn":
		thelog.Level = logrus.WarnLevel
	case "error":
		thelog.Level = logrus.ErrorLevel
	}

	// thelog.Formatter = &logrus.TextFormatter{FullTimestamp: false, DisableColors: true}

	if logToFile {
		logfilePath := determineLogfile()

		writer, err := rotatelogs.New(
			logfilePath+".%Y%m%d%H%M",
			// rotatelogs.WithLinkName(path),
			rotatelogs.WithMaxAge(24*30*time.Hour),
			rotatelogs.WithRotationTime(24*time.Hour),
		)
		if err != nil {
			panic("Unable to init log rotation")
		}

		lfsh := lfshook.NewHook(lfshook.WriterMap{
			logrus.InfoLevel:  writer,
			logrus.WarnLevel:  writer,
			logrus.ErrorLevel: writer,
		}, &logrus.JSONFormatter{})

		// lfsh.SetFormatter(&logrus.JSONFormatter{})

		thelog.Hooks.Add(lfsh)
	}
}

func New() *logrus.Entry {
	return thelog.WithFields(logrus.Fields{
		"_file": getCallerFilename(true),
		"_pid":  processId,
		"_env":  env,
	})
}

func getCallerFilename(shorten bool) string {
	_, fileName, _, ok := runtime.Caller(2)

	if ok && shorten {
		// Shorten the filename
		short := fileName
		for i := len(fileName) - 1; i > 0; i-- {
			if fileName[i] == '/' {
				short = fileName[i+1:]
				break
			}
		}
		fileName = short
	}

	return fileName
}

func determineLogfile() string {
	ldir := logdir

	executable := path.Base(os.Args[0])
	logname := fmt.Sprintf("%s.log", executable)

	// fmt.Printf("### executable %s\n", executable)

	cwd, err := os.Getwd()
	if err != nil {
		panic("Unable to get working directory")
	}

	if env == "local" {
		ldir = path.Join(cwd, "log")
	} else {
		_, err := os.Stat(ldir)
		if os.IsNotExist(err) {
			ldir = path.Join(cwd, "log")
		}
	}

	return path.Join(ldir, logname)
}
