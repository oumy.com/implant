package rev

/*
Interface to Rev.ai for speech-to-text conversion
*/

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
	"gitlab.com/oumy.com/implant/pkg/audio"
	"gitlab.com/oumy.com/implant/pkg/flag"
	"gitlab.com/oumy.com/implant/pkg/logger"
	"net/url"
)

var log = logger.New()

var (
	revHost        = flag.String("rev-host", "api.rev.ai", "Rev.ai STT host of service")
	revPath        = flag.String("rev-path", "/speechtotext/v1alpha/stream", "Rev.ai STT path endpoint")
	revAccessToken = flag.String("rev-access-token", "xxx", "Rev.ai access token")
)

type Engine struct {
	clientId         string
	ws               *websocket.Conn // connection to Rev
	writeLoopRunning bool
	readLoopRunning  bool
	audioInput       chan *audio.Buffer
	done             chan bool
	shutdownComplete chan bool
}

type RevResponse struct {
	Type       string `json:"type"`
	Final      bool   `json:"final"`
	Transcript string `json:"transcript"`
}

func New(clientId string, sampleRate int32) (*Engine, error) {
	query := fmt.Sprintf("access_token=%s&content_type=audio/x-raw;layout=interleaved;rate=%d;format=S16LE;channels=1", revAccessToken, sampleRate)
	u := url.URL{Scheme: "wss", Host: revHost, Path: revPath, RawQuery: query}

	ws, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.WithFields(logrus.Fields{
			"client": clientId,
			"error":  err,
		}).Error("Rev dial fail")

		return nil, err
	}

	engine := &Engine{
		clientId:         clientId,
		ws:               ws,
		done:             make(chan bool),
		shutdownComplete: make(chan bool),
		audioInput:       make(chan *audio.Buffer, 16),
	}

	go engine.readLoop()
	go engine.writeLoop()

	return engine, nil
}

func (engine *Engine) IsConnected() bool {
	return engine.readLoopRunning && engine.writeLoopRunning
}

func (engine *Engine) ProcessAudio(buf *audio.Buffer) {
	engine.audioInput <- buf
}

func (engine *Engine) readLoop() {
	defer engine.shutdownReadLoop()

	engine.readLoopRunning = true

	for {
		_, message, err := engine.ws.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.WithFields(logrus.Fields{
					"client": engine.clientId,
					"error":  err,
				}).Error("Rev read message fail, connection lost")
			}
			return
		}

		var response *RevResponse
		err = json.Unmarshal(message, &response)
		if err != nil {
			log.WithFields(logrus.Fields{
				"client": engine.clientId,
				"error":  err,
			}).Error("Rev unmarshall JSON fail")

			return
		}

		log.WithFields(logrus.Fields{
			"type":       response.Type,
			"final":      response.Final,
			"transcript": response.Transcript,
		}).Info("Rev Response")
	}
}

func (engine *Engine) shutdownReadLoop() {
	engine.readLoopRunning = false
}

func (engine *Engine) writeLoop() {
	defer engine.shutdownWriteLoop()

	engine.writeLoopRunning = true

	for {
		select {
		case <-engine.done:
			return
		case audioBuf := <-engine.audioInput:
			if audioBuf == nil {
				return
			}

			buf := new(bytes.Buffer)
			binary.Write(buf, binary.LittleEndian, audioBuf.Data)

			err := engine.ws.WriteMessage(websocket.BinaryMessage, buf.Bytes())
			if err != nil {
				log.WithFields(logrus.Fields{
					"client": engine.clientId,
					"error":  err,
				}).Error("Rev binary write to websocket fail")

				// exit the write loop on an error
				return
			}
		}
	}
}

func (engine *Engine) shutdownWriteLoop() {
	engine.writeLoopRunning = false

	// tell rev we are shutting down
	err := engine.ws.WriteMessage(websocket.TextMessage, []byte("eos"))
	if err != nil {
		log.WithFields(logrus.Fields{
			"client": engine.clientId,
			"error":  err,
		}).Error("Rev write EOS to websocket fail")
	}

	err = engine.ws.Close()
	if err != nil {
		log.WithFields(logrus.Fields{
			"client": engine.clientId,
			"error":  err,
		}).Error("Rev close websocket fail")
	}

	close(engine.shutdownComplete)
}

func (engine *Engine) Shutdown() {
	// we are just shutting down the write loop, which will close the socket, which will kill the read loop
	close(engine.done)

	<-engine.shutdownComplete
}
