package audio

import (
	"time"
)

type Buffer struct {
	Timestamp time.Time
	Data      []int16
}

func NewBuffer(ts time.Time, data []int16) *Buffer {
	return &Buffer{
		Timestamp: ts,
		Data:      data,
	}
}
