package mic

import (
	"github.com/gordonklaus/portaudio"
	"gitlab.com/oumy.com/implant/pkg/audio"
	"gitlab.com/oumy.com/implant/pkg/logger"
	"time"
)

var log = logger.New()

type Mic struct {
	buf         []int16
	stream      *portaudio.Stream
	frameLength int32
	bufDuration time.Duration
	done        chan bool
	AudioStream chan *audio.Buffer
}

func New(frameLength int32, sampleRate int32) (*Mic, error) {
	err := portaudio.Initialize()
	if err != nil {
		return nil, err
	}

	buf := make([]int16, frameLength)

	device, err := portaudio.DefaultInputDevice()
	if err == nil {
		log.Printf("Audio Input device: %s / %s", device.Name, device.HostApi.Name)
	}

	stream, err := portaudio.OpenDefaultStream(1, 0, float64(sampleRate), len(buf), buf)
	if err != nil {
		return nil, err
	}

	err = stream.Start()
	if err != nil {
		return nil, err
	}

	mic := &Mic{
		buf:         buf,
		stream:      stream,
		frameLength: frameLength,
		bufDuration: time.Second * time.Duration(frameLength) / time.Duration(sampleRate),
		done:        make(chan bool),
		AudioStream: make(chan *audio.Buffer, 16),
	}

	go mic.RunLoop()

	return mic, nil
}

func (mic *Mic) RunLoop() {
	defer mic.stop()

	log.Printf("Starting microphone read loop ... buffer duration %s", mic.bufDuration)

	ts := time.Now()

	for {
		select {
		case <-mic.done:
			return
		default:
			mic.stream.Read()

			data := make([]int16, mic.frameLength)
			copy(data, mic.buf)

			buf := audio.NewBuffer(ts, data)
			mic.AudioStream <- buf

			ts = ts.Add(mic.bufDuration)
		}
	}

}

func (mic *Mic) stop() {
	log.Printf("Shutting down microphone ...")

	close(mic.AudioStream)
	mic.stream.Stop()
	portaudio.Terminate()

	log.Printf("Microphone shut down")
}

func (mic *Mic) Shutdown() {
	mic.done <- true
}
